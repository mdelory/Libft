/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_test.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mdelory <mdelory@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/19 20:09:12 by mdelory           #+#    #+#             */
/*   Updated: 2019/01/29 01:31:21 by mdelory          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "vector.h"

#include <stdio.h>
#include <time.h>

void				test_print(void *e)
{
	printf("[%p] -> %d\n", *(int *)e, e);
}

int					test_cmp_max(void *e1, void *e2)
{
	if (*(int *)e1 > *(int *)e2)
		return (1);
	return (0);
}

int					test_cmp_min(void *e1, void *e2)
{
	if (*(int *)e1 < *(int *)e2)
		return (1);
	return (0);
}

int					main(int ac, char **av)
{
	t_vector		vec;
	int				*ptr;
	int				i;

	vector_init(&vec);
	srand(time(NULL));
	i = 0;
	while (i < 100)
	{
		ptr = malloc(sizeof(int));
		*ptr = rand();
		vector_add(&vec, ptr);
		i++;
	}
	vector_for_each(&vec, test_print);
	vector_sort(&vec, test_cmp_min);
	ft_putendl("--------------------------");
	vector_for_each(&vec, test_print);
	vector_sort(&vec, test_cmp_max);
	ft_putendl("--------------------------");
	vector_for_each(&vec, test_print);
	vector_free_all(&vec);
	vector_destroy(&vec);
	return (0);
}
